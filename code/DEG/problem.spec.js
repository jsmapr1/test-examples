import expect from 'expect';
import { sortTable } from './problem';

describe('sorting', () => {
  it('should sort by numbers', () => {
    const numbers = [90, 200, 10];
    const options = { order: 'ASC' };
    expect(sortTable(numbers, options)).toEqual([10, 90, 200]);
  });

  it('should sort sort strings with natural sort', () => {
    const words = ['AAA', 'CC', 'BBB'];
    const options = { order: 'ASC' };
    expect(sortTable(words, options)).toEqual(['AAA', 'BBB', 'CC']);
  });

  it('should sort by descending', () => {
    const numbers = [90, 200, 10];
    const options = { order: 'DSC' };
    expect(sortTable(numbers, options)).toEqual([200, 90, 10]);
  });

  it('should not convert empty string to an integer', () => {
    return;
    const words = ['BBB', '', 'CCC', 'AAA'];
    const options = { order: 'ASC' };
    expect(sortTable(words, options)).toEqual(['', 'AAA', 'BBB', 'CCC']);
  });

  it('should not mutate the array', () => {
    return;
    const numbers = [90, 200, 10];
    const options = { order: 'ASC' };
    sortTable(numbers, options);

    expect(numbers).toEqual([90, 200, 10]);
  });
});
