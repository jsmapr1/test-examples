import { redirect } from './routing';
import { getTaxInformation } from './taxService';

function formatPrice({ price, location }) {
  const user = document.getElementById('user').innerHTML;

  if (!location) {
    redirect('/');
    return;
  }

  const rate = getTaxInformation(location);
  const taxes = rate ? `plus $${price * rate} in taxes.` : 'plus tax.';

  const totalEl = document.getElementById('total');
  totalEl.innerHTML = `${user} your total is: ${price} ${taxes}`;
}

export { formatPrice };
