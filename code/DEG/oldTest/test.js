function formatPrice({ price, location }, user, getTaxInformation) {
  const rate = getTaxInformation(location);
  const taxes = rate ? `plus $${price * rate} in taxes.` : 'plus tax.';
  return `${user} your total is: ${price} ${taxes}`;
}

function applyPrice(item, getTaxInformation, domService) {
  const user = domService.get('user');
  const message = formatPrice(item, user, getTaxInformation);
  return domService.set('total', message);
}

export { applyPrice, formatPrice };
