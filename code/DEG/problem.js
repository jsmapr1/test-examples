import naturalCompare from 'string-natural-compare';

const numberCompare = (a, b) => {
  return a - b;
};

const sortTable = (rows, sorting) => {
  let sortOperation = numberCompare;
  const updated = rows.sort((a, b) => {
    if (isNaN(Number(b))) {
      // if (b ==='' || isNaN(Number(b))) {
      sortOperation = naturalCompare;
    }

    if (sorting.order === 'ASC') {
      return sortOperation(a, b);
    }
    return sortOperation(b, a);
  });

  return updated;
};

export { sortTable };
