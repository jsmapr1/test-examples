// Losing 0s from the table.
// null.toString() is an error
export function formatColumnData(column, columnData) {
  switch (column.format) {
    case 'currency':
      return `$ ${columnData}`;
    default:
      return columnData;
  }
}
