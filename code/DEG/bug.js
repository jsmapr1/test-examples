import { sortTable } from './problem';

const words = ['BBB', '', 'CCC', 'AAA'];
const options = { order: 'ASC' };

sortTable(words, options);
// ['BBB', '', 'AAA', 'CCC']
