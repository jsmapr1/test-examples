import expect from 'expect';
import { formatColumnData } from './homework';


describe('formatColumnData', () => {
  it('should convert currency', () => {
    expect(formatColumnData({ format: 'currency' }, 5)).toEqual('$ 5');
  });

  it('should return data without a format', () => {
    expect(formatColumnData({}, 5)).toEqual(5);
  });

  it('should convert 0 to a string', () => {
    return;
    expect(formatColumnData({}, 0)).toBe('0');
  });

  // 'should return empty string on null'
});
