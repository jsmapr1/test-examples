class FamilyTree {
  constructor() {
    this.family = {
      name: 'Doris',
      child: {
        name: 'Martha',
        child: {
          name: 'Dyan',
          child: {
            name: 'Bea',
          },
        },
      },
    };
  }
  * [Symbol.iterator]() {
    let node = this.family;
    while (node) {
      const current = node;
      node = node.child;
      yield current.name;
    }
  }
}

export default FamilyTree;
