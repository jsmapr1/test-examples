import expect from 'expect';
import FamilyTree from './generators';
import FamilyTreeProblem from './problem';

describe('should create an array of family members', () => {
  it('should get a list of family members with a method', () => {
    const family = new FamilyTreeProblem();
    expect(family.getMembers()).toEqual(['Doris', 'Martha', 'Dyan', 'Bea']);
  });
  it('should get a list of family members', () => {
    const family = new FamilyTree();
    expect([...family]).toEqual(['Doris', 'Martha', 'Dyan', 'Bea']);
  });
});
