/* eslint-disable */
import moment from 'moment';

class Coupon {
  constructor(price, expiration) {
    this.price = price;
    this.expiration = expiration || 'Two Weeks';
  }

  getPriceText() {
    return `$ ${this.price}`;
  }

  getExpirationMessage() {
    return `This offer expires in ${this.expiration}`;
  }

  isRewardsEligible(user) {
    return user.rewardsEligible && user.active;
  }

  getRewards(user) {
    if(this.authenticate(user)) {
      this.price = this.price * .9;
    }
  }
}

class FlashCoupon extends Coupon {
  constructor(price, expiration) {
    super(price);
    this.expiration = 'two hours'
  }

  getExpiration() {
    return `This is a flash offer and expires in ${this.expiration}`;
  }

  isRewardsEligible(user) {
    return super.isRewardsEligible(user) && this.price > 20;
  }

  getRewards() {
    if(this.isRewardsEligible(user)) {
      this.price = this.price * .8;
    }
  }
}

const isPremiumCustomer = () => true;
