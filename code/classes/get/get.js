class Coupon {
  constructor(price, expiration) {
    this._price = price;
    this.expiration = expiration || 'Two Weeks';
  }

  get priceText() {
    return `$${this._price}`;
  }

  get price() {
    return this._price;
  }

  set price(price) {
    this._price = parseInt(price, 10);
  }

  get expirationMessage() {
    return `This offer expires in ${this.expiration}`;
  }
}

export default Coupon;
