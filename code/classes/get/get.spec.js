import expect from 'expect';

import Coupon from './get';

describe('getters and setters', () => {
  it('should get a inner variable', () => {
    const coupon = new Coupon(10);
    expect(coupon.price).toEqual(coupon._price);
  });

  it('should set inner property', () => {
    const coupon = new Coupon(10);
    expect(coupon.price).toEqual(10);
    coupon.price = 100;
    expect(coupon.price).toEqual(100);
  });

  it('should parse int when setting', () => {
    const coupon = new Coupon(10);
    expect(coupon.price).toEqual(10);
    coupon.price = '100';
    expect(coupon.price).toEqual(100);
  });

  it('should get a message', () => {
    const coupon = new Coupon(10);
    expect(coupon.priceText).toEqual('$10');
  });
});
